import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import { Observable } from  'rxjs';
import { map } from  'rxjs/operators';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  profileForm = new FormGroup({
    userName: new FormControl(''),
    password: new FormControl(''),
    fullName: new FormControl(''),
    adress: new FormControl(''),
    mobile: new FormControl(''),
    email: new FormControl('',
      [Validators.required,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])
  });

  constructor(private http: HttpClient) { }
  PHP_API_SERVER = "http://127.0.0.1:8000/api/";
  isUserNameExists = false;
  ngOnInit(): void {
  }

  onkeyPress(event) {
    console.log(event.target.value);
    this.http.post('http://127.0.0.1:8000/api/checkUser', {"username": event.target.value}).subscribe(
      (response) =>{
        if(response["success"]) {
          this.isUserNameExists = true;
        } else{
          this.isUserNameExists = false;
        }
      },
      (error) => console.log(error)
    )
  }

  onSubmit() {

    console.warn(this.profileForm.value);

    var formData: any = new FormData();
    formData.append("userName", this.profileForm.get('userName').value);
    formData.append("password", this.profileForm.get('password').value);
    formData.append("fullName", this.profileForm.get('fullName').value);
    formData.append("adress", this.profileForm.get('adress').value);
    formData.append("mobile", this.profileForm.get('mobile').value);
    formData.append("email", this.profileForm.get('email').value);

    this.http.post('http://127.0.0.1:8000/api/people', formData).subscribe(
      (response) =>{
        if(response["success"]) {
          alert('success');
        } else{
          alert('Please Try Agian');
        }
      },
      (error) => console.log(error)
    )

  }

}
